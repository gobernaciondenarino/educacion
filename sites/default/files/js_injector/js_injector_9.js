var uri = decodeURI(jQuery(location).attr("href"));
var data = uri.split("/");
var code = data[4];
var fecha = new Date();
var año = fecha.getFullYear()+'';

jQuery.each(jQuery("input[id=edit-field-codigo-dane-value]"), function(idx, item){
	jQuery(item).val(code);
});

jQuery("#views-exposed-form-suma-personal-administrativo-block").find("#edit-field-codigo-dane-value--2").val(code);

jQuery("#edit-field-estud-a-o-tid option").filter(function() {

    return this.text == año; 
	
}).attr('selected', true);

jQuery("select[id=edit-field-anhos-tid] option").filter(function() {

	return this.text == año; 
	
}).attr('selected', true);

jQuery("select[id=edit-field-anhos-tid--2] option").filter(function() {

	return this.text == año; 
	
}).attr('selected', true);

jQuery(document).ready(function() {

 setTimeout(function(){
	jQuery("#block-views-46ff879704280ff6a5a45278eae78f55").show();
},4000);

	jQuery.each(jQuery("input[id=edit-field-codigo-dane-value]"), function(idx, item){
		jQuery(item).val(code);
	});

	jQuery("#edit-submit-suma-estudiantes").click();

	jQuery("#edit-submit-suma-personal-administrativo").click();
	
	jQuery("#edit-submit-suma-personal-docente").click();

	jQuery("#edit-field-rol-und").prop("disabled", true);

});

jQuery(document).ajaxComplete(function() {

	jQuery("#edit-field-comentario-und-0-value-counter").show();
	jQuery("#views-exposed-form-suma-estudiantes-block").find(".views-exposed-form").hide();
	jQuery("#views-exposed-form-suma-personal-administrativo-block").find(".views-exposed-form").hide();
        jQuery(".view-id-suma_personal_administrativo").find(".view-content").hide();
	jQuery("#views-exposed-form-suma-personal-docente-block").find(".views-exposed-form").hide();
	jQuery("#edit-field-rol-und").prop("disabled", true);
	jQuery("#edit-field-comentario-und-0-value").keyup(function(){
		if(jQuery("#edit-field-comentario-und-0-value").val().length > 2048){
			jQuery("#edit-submit--2").prop("disabled", true);
		}
		else
		{
			jQuery("#edit-submit--2").prop("disabled", false);
		}
	});

});
