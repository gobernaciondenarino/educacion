var uri = decodeURI(jQuery(location).attr("href"));
var data = uri.split("/");
var code = data[5];
var nombre = data[6];
var año = data[7];

jQuery("#edit-field-c-digo-dan-e-target-id").val(code);
jQuery("#edit-title").val(nombre);
jQuery("#edit-field-estud-a-o-tid option").filter(function() {
    return this.text == año; 
}).attr('selected', true);

jQuery("#edit-field-anhos-tid option").filter(function() {
	return this.text == año; 
}).attr('selected', true);

jQuery("#edit-field-c-digo-dan-e-target-id").attr("disabled", true);
jQuery("#edit-title").attr("disabled", true);

jQuery(document).ready(function() {

	jQuery("#views-exposed-form-personal-administrativo-block").find(".views-exposed-form").hide(); 
	jQuery("#views-exposed-form-personal-docente-block").find(".views-exposed-form").hide(); 

	jQuery("button[id=edit-submit-personal-administrativo]").click();
	jQuery("button[id=edit-submit-personal-docente]").click();

	jQuery("#edit-submit-detalle-de-personal").click(function() {

		var saño = jQuery("#edit-field-estud-a-o-tid option:selected").text();

		jQuery("#edit-field-anhos-tid option").filter(function() {
		
			return this.text == saño; 
			
		}).attr('selected', true);
		
		jQuery("button[id=edit-submit-personal-administrativo]").click();
		jQuery("button[id=edit-submit-personal-docente]").click();

	});
	
});

jQuery(document).ajaxComplete(function() {

	jQuery("#views-exposed-form-personal-administrativo-block").find(".views-exposed-form").hide(); 
	jQuery("#views-exposed-form-personal-docente-block").find(".views-exposed-form").hide(); 
	
	
	var uri = decodeURI(jQuery(location).attr("href"));
	var data = uri.split("/");
	var code = data[5];
	var nombre = data[6];
	
	jQuery("#edit-field-c-digo-dan-e-target-id").val(code);
	jQuery("#edit-title").val(nombre);

	jQuery("#edit-field-c-digo-dan-e-target-id").attr("disabled", true);
	jQuery("#edit-title").attr("disabled", true);
	
});