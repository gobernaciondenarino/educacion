jQuery("#prev").parents(".odd").find(".views-field-created").html();

jQuery(document).ready(function(){

	jQuery(".pane-better-comments-admin-comment-preview").before("<div class='popupC'><div class='popupCC'><img class='iconExit' src='/sites/default/files/exit.png' /><p id='dateComment'></p><p id='schoolComment'></p><p id='autorComment'></p></div></div>");
	jQuery(".pane-better-comments-admin-comment-preview").appendTo(".popupCC");
	jQuery(".iconExit").click(function(){
		jQuery(".popupC").hide();
	});
	
	jQuery(".ajax-processed").each(function(){
		if(jQuery(this).html() == "Pre visualizar"){
			jQuery(this).click(function(){
			
				var dateComment = jQuery(this).parents(".odd").find(".views-field-created").html();
				if(!dateComment)
					dateComment = jQuery(this).parents(".even").find(".views-field-created").html();
				jQuery("#dateComment").html("Fecha del comentario: " + dateComment);
				
				var schoolComment = jQuery(this).parents(".odd").find(".views-field-title").html();
				if(!schoolComment)
					schoolComment = jQuery(this).parents(".even").find(".views-field-title").html();				
				jQuery("#schoolComment").html("Escuela: " + schoolComment);
				
				var autorComment = jQuery(this).parents(".odd").find(".views-field-name").html();
				if(!autorComment)
					autorComment = jQuery(this).parents(".even").find(".views-field-name").html();				
				jQuery("#autorComment").html("Autor: " + autorComment);
				
					setTimeout(function(){
						jQuery(".popupC").show();
					},2000);
			});
		}
	});
	
});