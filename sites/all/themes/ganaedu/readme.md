# PREREQUISITES
1. For Windows users: Visual C++ 2015 Build Tools, Phyton 2.7
More info: https://github.com/nodejs/node-gyp#on-windows

# AUTOMATIC TASK FOR DEVELOPMET

1. Install stable version of Nodejs
2. Run `npm install` to install dependencies
3. Run `npm install -g grunt-cli` to install grunt-cli
4. Run `grunt` to watch files

# LIVERELOAD

1. Add js into drupal `http://localhost:35729/livereload.js`
2. You can use chrome extension `User JavaScript and CSS` and type JavaScript code:

(function(){
console.log('Livereload...');
jQuery('body').append('<script type="text/javascript" src="http://localhost:35729/livereload.js"></script>');
})();

3. Modify sass file and save it
4. You wait a few seconds to automatic change styles

# DRUPAL CHATROOM

1. Go into **node_modules/drupal-node.js**
2. Rename **nodejs.config.js.example** to **nodejs.config.js**
3. Set **serviceKey** in **nodejs.config.js** and **admin/config/nodejs/config** in Drupal
4. Open **node_modules/drupal-node.js** in terminal and type **node app.js**
More info: https://github.com/beejeebus/drupal-nodejs