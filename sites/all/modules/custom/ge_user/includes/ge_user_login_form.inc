<?php

function formulario_logueo($form, &$form_state) {
  $module = drupal_get_path('module', 'ge_user');
  drupal_add_library('system', 'drupal.ajax');
  ctools_include('modal');
  ctools_modal_add_js();
  drupal_add_js($module . '/ge_user.js');
  
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => true,
    '#maxlength' => EMAIL_MAX_LENGTH,
    '#size' => 15,
    '#default_value' => !empty($form_state['values']['email']) ? $form_state['values']['email'] : ''
  );
  $form['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => true,
    '#maxlength' => 120,
    '#size' => 15,
    '#default_value' => !empty($form_state['values']['pass']) ? $form_state['values']['pass'] : ''
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#prefix' => '<p class="text-center">',
    '#suffix' => '</p>',
    '#value' => '<span class="glyphicon glyphicon-check"></span> Iniciar sesión',
  );
  $form['footer'] = array(
    '#markup' => theme('user_login_footer')
  );
  return $form;
}

function formulario_logueo_validate($form, &$form_state) {
  //echo die('<pre>'.print_R($form_state['values'], true). '</pre>');
  
  if(!empty($form_state['values']['email']) && !empty($form_state['values']['pass'])){
    if(!filter_var($form_state['values']['email'], FILTER_VALIDATE_EMAIL)){
      form_set_error('email', 'El Correo electrónico ingresado es incorrecto.');
    }else{
      $user = user_load_by_mail($form_state['values']['email']);
      if($user){
        $logindata = array(
          'name' => $user->name,
          'pass' => $form_state['values']['pass']
        );
        $uid = user_authenticate($user->name, $form_state['values']['pass']);
        if($uid){
          if(!user_is_blocked($user->name)){
            $logindata['uid'] = $uid;
            user_login_submit(array(), $logindata);
          }else{
            form_set_error('email', 'Debe activar su cuenta para iniciar sesión.');
          }
        }else{
          form_set_error('pass', 'La contraseña que ha ingresado es incorrecta.');
        }
      }else{
        form_set_error('pass', 'El correo electrónico ingresado no existe.');
      }
    }
  }
}

function formulario_logueo_submit($form, &$form_state) {
  drupal_goto('/');
}


function formulario_reenviar_correo_response($type = 'ajax') {
  if ($type == 'ajax') {
    ctools_include('modal');
    ctools_modal_add_js();
    $commands = array();
    

    $form = drupal_get_form('formulario_activacion');
    $output = render($form);
    
    $commands[] = ctools_modal_command_display('Reenviar mensaje de activación', theme('status_messages') . $output);
    $commands[] = array('command' => 'set_activation_email');
    $page = array('#type' => 'ajax', '#commands' => $commands);
    ajax_deliver($page);
  }
  else {
    $output = t("This is some content delivered via a page load.");
    return $output;
  }
}

function formulario_activacion($form, &$form_state) {
  global $user;
  
  module_load_include('inc', 'user_email_verification', 'user_email_verification.admin');

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 60,
    '#maxlength' => EMAIL_MAX_LENGTH,
    '#required' => TRUE,
    '#default_value' => isset($_GET['name']) ? $_GET['name'] : '',
  );
  // Allow logged in users to request this also.
  if ($user->uid > 0) {
    $form['name']['#type'] = 'value';
    $form['name']['#value'] = $user->mail;
    $form['mail'] = array(
      '#prefix' => '<p>',
      '#markup' =>  t('Verify email will be send to %email.', array('%email' => $user->mail)),
      '#suffix' => '</p>',
    );
  }
  $form['actions'] = array(
    '#prefix' => '<p class="text-center">',
    '#suffix' => '</p>'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => '<span class="glyphicon glyphicon-envelope"></span> Enviar'
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#attributes' => array(
      'class' => array('btn-error ctools-close-modal')
    ),
    '#value' => 'Cancelar'
  );
  
  return $form;
}

function formulario_activacion_validate($form, &$form_state) {
  module_load_include('inc', 'user_email_verification', 'user_email_verification.admin');
  $name = trim($form_state['values']['name']);
  // Try to load by email.
  $users = user_load_multiple(array(), array('mail' => $name, 'status' => '0'));
  $account = reset($users);
  if (!$account) {
    // No success, try to load by name.
    $users = user_load_multiple(array(), array('name' => $name, 'status' => '0'));
    $account = reset($users);
  }
  if (isset($account->uid)) {
    form_set_value(array('#parents' => array('account')), $account, $form_state);
  }
  else {
    form_set_error('name', t('Sorry, %name is not recognized as a user name or an e-mail address.', array('%name' => $name)));
  }
}

function formulario_activacion_submit($form, &$form_state) {
  module_load_include('inc', 'user_email_verification', 'user_email_verification.admin');
  global $language;
  
  $account = $form_state['values']['account'];
  // Mail one time login URL and instructions using current language.
  $mail = user_email_verification_mail_notify('verify', $account, $language);
  if (!empty($mail)) {
    watchdog('user', 'Verification instructions mailed to %name at %email.', array('%name' => $account->name, '%email' => $account->mail));
    drupal_set_message(t('El mensaje de activación ha sido enviado a su correo electrónico.'));
  }
  
}

function formulario_recuperar_clave_response($type = 'ajax') {
  
  module_load_include('inc', 'user', 'user.pages');
  if ($type == 'ajax') {
    ctools_include('modal');
    ctools_modal_add_js();
    $commands = array();
    
    
    $form = drupal_get_form('formulario_recuperar_clave');
    $output = render($form);
    
    $commands[] = ctools_modal_command_display('Recuperar contraseña', theme('status_messages') . $output);
    $commands[] = array('command' => 'set_activation_email');
    $page = array('#type' => 'ajax', '#commands' => $commands);
    ajax_deliver($page);
  }
  else {
    $output = t("This is some content delivered via a page load.");
    return $output;
  }
}

function formulario_recuperar_clave() {
  global $user;
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#size' => 60,
    '#maxlength' => max(USERNAME_MAX_LENGTH, EMAIL_MAX_LENGTH),
    '#required' => TRUE,
    '#default_value' => isset($_GET['name']) ? $_GET['name'] : '',
  );
  // Allow logged in users to request this also.
  if ($user->uid > 0) {
    $form['name']['#type'] = 'value';
    $form['name']['#value'] = $user->mail;
    $form['mail'] = array(
      '#prefix' => '<p>',
      '#markup' =>  t('Password reset instructions will be mailed to %email. You must log out to use the password reset link in the e-mail.', array('%email' => $user->mail)),
      '#suffix' => '</p>',
    );
  }
  $form['actions'] = array(
    '#prefix' => '<p class="text-center">',
    '#suffix' => '</p>'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => '<span class="glyphicon glyphicon-envelope"></span> Enviar'
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#attributes' => array(
      'class' => array('btn-error ctools-close-modal')
    ),
    '#value' => 'Cancelar'
  );
  
  return $form;
}

function formulario_recuperar_clave_validate($form, &$form_state) {
  $name = trim($form_state['values']['name']);
  // Try to load by email.
  $users = user_load_multiple(array(), array('mail' => $name, 'status' => '1'));
  $account = reset($users);
  if (!$account) {
    // No success, try to load by name.
    $users = user_load_multiple(array(), array('name' => $name, 'status' => '1'));
    $account = reset($users);
  }
  if (isset($account->uid)) {
    form_set_value(array('#parents' => array('account')), $account, $form_state);
  }
  else {
    form_set_error('name', t('Sorry, %name is not recognized as a user name or an e-mail address.', array('%name' => $name)));
  }
}

function formulario_recuperar_clave_submit($form, &$form_state) {
  global $language;
  
  $account = $form_state['values']['account'];
  // Mail one time login URL and instructions using current language.
  $mail = _user_mail_notify('password_reset', $account, $language);
  if (!empty($mail)) {
    watchdog('user', 'Password reset instructions mailed to %name at %email.', array('%name' => $account->name, '%email' => $account->mail));
    drupal_set_message(t('Further instructions have been sent to your e-mail address.'));
  }
}


function ge_user_email_verification_verify($form, &$form_state, $uid, $timestamp, $hashed_pass) {
  // Time out, in seconds, until login URL expires. Defaults to 24 hours =
  // 86400 seconds.
  $timeout = variable_get('user_email_verification_validate_interval', 86400);
  $current = REQUEST_TIME;
  // Some redundant checks for extra security ?
  $users = user_load_multiple(array($uid), array('status' => '0'));
  
  if ($timestamp <= $current && $account = reset($users)) {
    // No time out for first time login.
    if ($current - $timestamp > $timeout) {
      drupal_set_message('Ha intentado utilizar un enlace de verificación único que ha caducado. Por favor, solicite uno nuevo.');
      drupal_goto('usuario/ingresar');
    }
    elseif ($account->uid) {
      $verified = user_email_verification_load_verify_flag($account->uid);
      if (!$verified && ($hashed_pass == user_email_verification_hmac($account->uid, $timestamp))) {
        db_update('user_email_verification')
        ->fields(array(
          'verified' => 1,
        ))
        ->condition('uid', $account->uid, '=')
        ->execute();
        
        drupal_set_message('La cuenta ha sido activada.');
        watchdog('user', 'User "%name" verified e-mail address "%email".', array('%name' => $account->name, '%email' => $account->mail));
        
        if ($account->status == 0) {
          db_update('users')
          ->fields(array(
            'status' => 1,
          ))
          ->condition('uid', $account->uid, '=')
          ->execute();
        }
        drupal_goto('usuario/ingresar');
        
        
        if (module_exists('rules')) {
          // Invoke rules event
          rules_invoke_event('user_email_verification_verified_email', $account);
        }
      }
      else {
        drupal_set_message('Ha intentado utilizar un enlace de verificación único que se ha utilizado o ya no es válido. Por favor, solicite uno nuevo.');
        drupal_goto('usuario/ingresar');
      }
    }
    else {
      drupal_set_message('Ha intentado utilizar un enlace de verificación único que se ha utilizado o ya no es válido. Por favor, solicite uno nuevo.');
      drupal_goto('usuario/ingresar');
    }
  }
  else {
    // Deny access, no more clues.
    // Everything will be in the watchdog's URL for the administrator to check.
    drupal_access_denied();
    drupal_exit();
  }
}

function formulario_recuperar_clave_finalizar($form, &$form_state, $uid, $timestamp, $hashed_pass, $action = NULL) {
  global $user;
  
  // When processing the one-time login link, we have to make sure that a user
  // isn't already logged in.
  if ($user->uid) {
    // The existing user is already logged in. Log them out and reload the
    // current page so the password reset process can continue.
    if ($user->uid == $uid) {
      // Preserve the current destination (if any) and ensure the redirect goes
      // back to the current page; any custom destination set in
      // hook_user_logout() and intended for regular logouts would not be
      // appropriate here.
      $destination = array();
      if (isset($_GET['destination'])) {
        $destination = drupal_get_destination();
      }
      user_logout_current_user();
      unset($_GET['destination']);
      drupal_goto(current_path(), array('query' => drupal_get_query_parameters() + $destination));
    }
    // A different user is already logged in on the computer.
    else {
      $reset_link_account = user_load($uid);
      if (!empty($reset_link_account)) {
        drupal_set_message(t('Another user (%other_user) is already logged into the site on this computer, but you tried to use a one-time link for user %resetting_user. Please <a href="!logout">logout</a> and try using the link again.',
          array('%other_user' => $user->name, '%resetting_user' => $reset_link_account->name, '!logout' => url('user/logout'))), 'warning');
      } else {
        // Invalid one-time link specifies an unknown user.
        drupal_set_message(t('The one-time login link you clicked is invalid.'), 'error');
      }
      drupal_goto();
    }
  }
  else {
    // Time out, in seconds, until login URL expires. Defaults to 24 hours =
    // 86400 seconds.
    $timeout = variable_get('user_password_reset_timeout', 86400);
    $current = REQUEST_TIME;
    // Some redundant checks for extra security ?
    $users = user_load_multiple(array($uid), array('status' => '1'));
    if ($timestamp <= $current && $account = reset($users)) {
      // No time out for first time login.
      if ($account->login && $current - $timestamp > $timeout) {
        drupal_set_message(t('You have tried to use a one-time login link that has expired. Please request a new one using the form below.'), 'error');
        drupal_goto('user/password');
      }
      elseif ($account->uid && $timestamp >= $account->login && $timestamp <= $current && $hashed_pass == user_pass_rehash($account->pass, $timestamp, $account->login, $account->uid)) {
        // First stage is a confirmation form, then login
        if ($action == 'login') {
          // Set the new user.
          $user = $account;
          // user_login_finalize() also updates the login timestamp of the
          // user, which invalidates further use of the one-time login link.
          user_login_finalize();
          watchdog('user', 'User %name used one-time login link at time %timestamp.', array('%name' => $account->name, '%timestamp' => $timestamp));
          drupal_set_message(t('You have just used your one-time login link. It is no longer necessary to use this link to log in. Please change your password.'));
          // Let the user's password be changed without the current password check.
          $token = drupal_random_key();
          $_SESSION['pass_reset_' . $user->uid] = $token;
          drupal_goto('user/' . $user->uid . '/edit', array('query' => array('pass-reset-token' => $token)));
        }
        else {
          $form['message'] = array('#markup' => t('<p>This is a one-time login for %user_name and will expire on %expiration_date.</p><p>Click on this button to log in to the site and change your password.</p>', array('%user_name' => $account->mail, '%expiration_date' => format_date($timestamp + $timeout))));
          $form['help'] = array('#markup' => '<p>' . t('This login can be used only once.') . '</p>');
          $form['actions'] = array('#type' => 'actions');
          $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Log in'));
          $form['#action'] = url("user/reset/$uid/$timestamp/$hashed_pass/login");
          return $form;
        }
      }
      else {
        drupal_set_message(t('You have tried to use a one-time login link that has either been used or is no longer valid. Please request a new one using the form below.'), 'error');
        drupal_goto('user/password');
      }
    }
    else {
      // Deny access, no more clues.
      // Everything will be in the watchdog's URL for the administrator to check.
      drupal_access_denied();
      drupal_exit();
    }
  }
}

