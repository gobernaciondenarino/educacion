<div class="olvido_contrasena">
<p> 
    <a href="/usuario/recuperar-contrasena" class="use-ajax">¿Olvidó su contraseña?</a> 
  </p>
</div>


<div class="recovery-password text-center">
    <p style="display:none" >Si no le ha llegado el mensaje de activación a su correo, presione en el botón reenviar</p>
  <p>
    <a class="btn btn-default use-ajax" href="/usuario/reenviar-correo">
      <span class="glyphicon glyphicon-envelope"></span> Reenviar mensaje de activación
    </a>
    <a class="btn btn-success" href="<?php print $base_url ?>/usuario/registrarme">
      <span class="glyphicon glyphicon-user"></span> Crear una cuenta
    </a>
  </p>
</div>