<?php if($account->uid): ?>

<ul>
  <?php if(user_access('administer site dashboard')): ?>
  <li>
    <a href="/admin/config/administration/dashboard" title="Acceder al dashboard">
      <span class="glyphicon glyphicon-dashboard"></span> Dashboard
    </a>
  </li>
  <?php endif; ?>
  <li>
    <a href="/user/<?php print $user->uid ?>/edit" title="Editar mi cuenta" class="nombre_c">
      <span class="glyphicon glyphicon-user"></span> <?php print $account->field_nombres['und'][0]['value'] ?>
    </a>
  </li>
    </li>
     <li>
    <a href="/dashboard/contáctanos" title="Contáctenos">
      <span class="glyphicon glyphicon-comment"></span> Contáctenos
    </a>
  </li>
  <li>
    <a href="/user/logout" title="Cerrar sesión">
      <span class="glyphicon glyphicon-remove"></span> Cerrar sesión
    </a>
  </li>
</ul>
<?php else: ?>
<ul>
  <li>
    <a href="/usuario/ingresar" title="Iniciar sesión">
      <span class="glyphicon glyphicon-user"></span> Iniciar sesión
    </a>
  <li>
    <a href="/usuario/registrarme" title="Crear cuenta">
      <span class="glyphicon glyphicon-share"></span> Crear cuenta
    </a>
  </li>
   <li>
    <a href="/dashboard/contáctanos" title="Contáctenos">
      <span class="glyphicon glyphicon-comment"></span> Contáctenos
    </a>
  </li>
 </ul>


<?php endif; ?>