<style>
  .encuestasTitulo{
    background: #07A4E7;
    color: #ffffff;
    padding-left: 2%;
    padding-right: 2%;
    padding-bottom: 1%;
    padding-top: 1%;
    font-size: 15px;
  }
  .encuestasPregunta{
    float: left;
    margin-right: 3%;
    width: 47%;
  }
  .encuestasPromedioResult{
    background: #1CA344;
    padding-top: 1%;
    padding-bottom: 1%;
    padding-left: 1%;
    padding-right: 1%;
    width: 100%;
    color: #ffffff;
    font-size: 13px;
    font-weight: bold;
  }
</style>

<?php 

  $totaPreguntas = 0;
  $sumaTotal = 0 ;
  foreach($titlePreguntasArray as $preguntas){$totaPreguntas = $totaPreguntas + 1;}
  echo "<br/><span class='titleSchool'>".$school."</span>";
  echo "<form method='post'>";
  foreach($titlePreguntasArray as $preguntas){
    $sumaTotal = $sumaTotal + $preguntas[3];
    echo "<div class='encuestasPregunta'>";
    echo "<div class='encuestasTitulo'>".$preguntas[0]."</div>";
    echo "<p>".$preguntas[1]."</p>";
    echo "<p>".$preguntas[2]."</p>";
    echo "<hr>";
    echo "<p><b>Total de usuarios que respondieron: </b>".$preguntas[3]."</p>";
    echo "<p><b>Promedio total de respuesta: </b>".number_format($preguntas[4] / $preguntas[3],1)."</p>";
    echo "</br></div>";
  }
  echo "<div class='encuestasPromedio' id='promedio'><span class='encuestasPromedioResult'>En promedio, el total de la encuesta es : <font size='4'>".number_format($sumaTotal/$totaPreguntas,1)."</font></span></div>";
  echo "</form>";

?>