<style>
	.tableInforme{
		width:100%;
	}
	.tableInforme td{
		padding:3%;
		border-bottom:1px solid;
		border-color:#dddddd;
	}
	.promTotal{
		margin-top:5%;
	}
</style>

<script>
	
	//	Funcion para ver detalle de una pregunta
	function detailPregunta(id){
		jQuery("#pregunta").val(id);
		jQuery("#formInforme").submit();
	}
	
	//	Volver
	function back(){
		jQuery("#pregunta").val(0);
		jQuery("#formInforme").submit();
	}
	
</script>

<!--	Título	-->
<p>
	<b>Consultar Encuestas Diligenciadas</b>
</p>

<!--	Filtros	-->
<form id="formInforme" action="#" method="post">
	<input type="hidden" id="pregunta" name="pregunta" value="<?=$pregunta?>" />
	<p>Seleccionar encuesta:</p>
	<p>
		<select id="encuesta" name="encuesta">
			<option value="">- Seleccionar - </option>
			<?php
				foreach($encuestas as $encuestaArray){
					if($encuesta == $encuestaArray->id)
						echo "<option selected value='".$encuestaArray->id."'>".$encuestaArray->name."</option>";
					else
						echo "<option value='".$encuestaArray->id."'>".$encuestaArray->name."</option>";
				}
			?>
		</select>
	</p>
	<p>Seleccionar escuela:</p>
	<p>
		<select id="escuela" name="escuela">
			<option value=""></option>
			<option value="todas" <?php if($escuela=='todas'){echo "selected";}?>>Todas las escuelas</option>
			<?php
				foreach($escuelas as $escuelaArray){
					
					//	Consultar nombre de la escuela seleccionada
					$schoolCons = db_query("SELECT title_field_value FROM field_data_title_field where entity_id in (select entity_id from field_revision_field_codigo_dane where field_codigo_dane_value = '".$escuelaArray->school."' )");
					foreach($schoolCons as $schoolCon){
						$titleSchool = $schoolCon->title_field_value;
					}
					
					if($escuela == $escuelaArray->school)
						echo "<option selected value='".$escuelaArray->school."'>".$titleSchool."</option>";
					else
						echo "<option value='".$escuelaArray->school."'>".$titleSchool."</option>";
				}
			?>
		</select>
	</p>
	<?php if(!$pregunta){echo "<button type='submit'>Consultar</button>";} ?>
</form>

<br/>

<!--	Consulta general	-->
<?php
	if($encuesta && $escuela && !$pregunta){
		
		//	Listar preguntas
		$promedioTotal = 0;
		$contTotal = 0;
		$sumaTotal = 0;
		$contPregunta = 0;
		$preguntasGrafico = "";
		$promedioGraficos = "";
		$graficoColors = "";
		$consPreguntas = db_query("select id,name from encuestasPreguntas where idEncu = '$encuesta' ");
		echo "<table class='tableInforme'>";
		echo "<tr>";
		echo "<td width='40%'><b>Pregunta</b></td>";
		echo "<td width='20%'><b>Promedio</b></td>";
		echo "<td width='20%'><b>Cantidad de usuarios</b></td>";
		echo "<td width='20%'></td>";
		echo "</tr>";
		foreach($consPreguntas as $preguntaArray){
			$contPregunta = $contPregunta + 1;
			
			//	Obtener promedio de la pregunta
			$contPromedio = 0;
			$sumPromedio = 0;
			$promedio = 0;
			
			if($escuela == "todas"){
				$promedioCons = db_query("select respuesta from encuestasRespuestas where encuesta = '".$encuesta."' and pregunta = '".$preguntaArray->id."'");
			}else{
				$promedioCons = db_query("select respuesta from encuestasRespuestas where encuesta = '".$encuesta."' and school = '".$escuela."' and pregunta = '".$preguntaArray->id."'");
			}
			
			foreach($promedioCons as $respuestaArray){
				$contPromedio = $contPromedio + 1;
				$sumPromedio = $sumPromedio + $respuestaArray->respuesta;
			}
			@$promedio = $sumPromedio/$contPromedio;
			$promedio = number_format($promedio,0);
			$sumaTotal = $sumaTotal + $sumPromedio;
			$contTotal = $contTotal + $contPromedio;
			
			if($promedio == "nan")
				$promedio = 0;
			
			if($preguntasGrafico){
				$preguntasGrafico = $preguntasGrafico.",'Pregunta ".$contPregunta."'";
				$promedioGraficos = $promedioGraficos.",".$promedio;
				
				if($promedio >= $bajaDesde && $promedio <= $bajaHasta)
					$graficoColors = $graficoColors.",'".$bajaColor."'";
				
				if($promedio >= $mediaDesde && $promedio <= $mediaHasta)
					$graficoColors = $graficoColors.",'".$mediaColor."'";
				
				if($promedio >= $altaDesde && $promedio <= $altaHasta)
					$graficoColors = $graficoColors.",'".$altaColor."'";
				
			}else{
				$preguntasGrafico = "'Pregunta ".$contPregunta."'";
				$promedioGraficos = $promedio;
				
				if($promedio >= $bajaDesde && $promedio <= $bajaHasta)
					$graficoColors = "'".$bajaColor."'";	
				
				if($promedio >= $mediaDesde && $promedio <= $mediaHasta)
					$graficoColors = "'".$mediaColor."'";
				
				if($promedio >= $altaDesde && $promedio <= $altaHasta)
					$graficoColors = "'".$altaColor."'";
				
			}

			echo "<tr>";
			echo "<td>".$contPregunta.". ".$preguntaArray->name."</td>";
			echo "<td><center>".$promedio."</center></td>";
			echo "<td><center>".$contPromedio."</center></td>";
			
			//	Unicamente se ve el detalle cuando se seleccionan todas las escuelas
			if($escuela == "todas")
				echo "<td><button class='buttonDetail' onclick=\"detailPregunta('".$preguntaArray->id."');\">Ver detalle</button></td>";
			
			echo "</tr>";
		}
		
		echo "</table>";
		
		//	Promedio de la encuesta
		@$promedioTotal = $sumaTotal / $contTotal;
		$promedioTotal = number_format($promedioTotal,0);
		if($promedioTotal == "nan")
			$promedioTotal = 0;
		
		if($promedioTotal >= $bajaDesde && $promedioTotal<=$bajaHasta)
			$backColor = $bajaColor;
		
		if($promedioTotal >= $mediDesde && $promedioTotal<=$mediaHasta)
			$backColor = $mediaColor;
		
		if($promedioTotal >= $altaDesde && $promedioTotal<=$altaHasta)
			$backColor = $altaColor;
		
		echo "<center><p class='promTotal' style='background:$backColor;' ><b>Promedio de la encuesta:</b> ".$promedioTotal."</p></center>";
		
		?>
		
		<!--	Gráfico	-->
		<div id="container" style="width: 75%;">
			<canvas id="canvas"></canvas>
		</div>
		<script>
			var MONTHS = [<?=$preguntasGrafico?>];
			var color = Chart.helpers.color;
			var barChartData = {
				labels: [<?=$preguntasGrafico?>],
				datasets: [{
					label: "Baja",
					backgroundColor: [<?=$graficoColors?>],
					borderWidth: 1,
					data: [
						<?=$promedioGraficos?>
					]
				},{
					label: "Media",
					backgroundColor: "<?=$mediaColor?>",
				},{
					label: "Alta",
					backgroundColor: "<?=$altaColor?>",
				}]
			};
			window.onload = function(){
				var ctx = document.getElementById('canvas').getContext('2d');
				window.myBar = new Chart(ctx, {
					type: 'bar',
					data: barChartData,
					options: {
						responsive: true,
						legend: {
							position: 'top',
						},
						title: {
							display: true,
							text: 'Chart.js Bar Chart'
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								}
							}]
						}
					}
				});

			};
		</script>
		
		<?php

	}
	
?>

<!--	Consulta por pregunta	-->
<?php

	if($pregunta){
		
		//	Desactivar filtros
		echo "<script>jQuery('#encuesta').attr('disabled',true);jQuery('#escuela').attr('disabled',true);</script>";
		
		//	Volver
		echo "<button onclick='back();'>Volver al informe</button><br/><br/>";
		
		//	Consultar información de la pregunta
		$preguntaCons = db_query("select * from encuestasPreguntas where id = '$pregunta' ");
		foreach($preguntaCons as $preguntaArray){
			echo "<p><b>".$preguntaArray->name."</b></p>";
			echo "<p>".$preguntaArray->description."</p>";
			echo "<p>".$preguntaArray->opciones."</p>";
		}
		
		//	Consultar calificaciones
		$promedio = 0;
		$sumPromedio = 0;
		$contPromedio = 0;
		$usuarios = "";
		$calificaciones = "";
		$calificacionesCons = db_query("select * from encuestasRespuestas where pregunta = '$pregunta'");
		
		echo "<table class='tableInforme'>";
		echo "<tr>";
		
		if($escuela == "todas"){
			echo "<td><b>Escuela</b></td>";
			echo "<td><b>Promedio</b></td>";
		}else{
			echo "<td><b>Usuario</b></td>";
			echo "<td><b>Calificación</b></td>";
			echo "<td><b>Fecha</b></td>";
		}
		
		echo "</tr>";
		
		if($escuela != "todas"){
			foreach($calificacionesCons as $calificacionArray){
				
				//	Obtener nombre del usuario
				$usuaCons = db_query("select field_nombres_value from field_data_field_nombres where entity_id = '".$calificacionArray->user."' ");
				foreach($usuaCons as $usuaCon){
					$usuario = $usuaCon->field_nombres_value;
				}
				
				$contPromedio = $contPromedio + 1;
				$sumPromedio = $sumPromedio + $calificacionArray->respuesta;
				
				if($usuarios){
					$usuarios = $usuarios.",'".$usuario."'";
					$calificaciones = $calificaciones.",".$calificacionArray->respuesta;
				}else{
					$usuarios = "'".$usuario."'";
					$calificaciones = $calificacionArray->respuesta;
				}
				
				echo "<tr>";
				echo "<td>".$usuario."</td>";
				echo "<td>".$calificacionArray->respuesta."</td>";
				echo "<td>".$calificacionArray->register."</td>";
				echo "</tr>";
				
			}
		}else{
			
			$promedio = 0;
			$contPromedio = 0;
			$sumPromedio = 0;
			$usuarios = "";
			$calificaciones = "";
			
			//	Consultar escuelas
			$consEscuResp = db_query("SELECT school,sum(respuesta)/count(*) as promedio FROM `encuestasRespuestas` where encuesta = $encuesta and pregunta = $pregunta group by 1");
			foreach($consEscuResp as $encuRespArray){
				
				//	Consultar nombre de la escuela seleccionada
				$schoolCons = db_query("SELECT title_field_value FROM field_data_title_field where entity_id in (select entity_id from field_revision_field_codigo_dane where field_codigo_dane_value = '".$encuRespArray->school."' )");
				foreach($schoolCons as $schoolCon){
					$titleSchool = $schoolCon->title_field_value;
				}
				$contPromedio = $contPromedio + 1;
				echo "<tr>";
				echo "<td>".$contPromedio.".".$titleSchool."</td>";
				echo "<td>".number_format($encuRespArray->promedio,0)."</td>";
				echo "</tr>";
				
				$sumPromedio = $sumPromedio + $encuRespArray->promedio;
				
				if($usuarios){
					$usuarios = $usuarios.",'".$contPromedio."'";
					$calificaciones = $calificaciones.",".number_format($encuRespArray->promedio,0);
				}else{
					$usuarios = "'".$contPromedio."'";
					$calificaciones = number_format($encuRespArray->promedio,0);
				}
				
			}
			
		}
		
		echo "</table>";
		
		//	Promedio de la pregunta
		$promedio = $sumPromedio / $contPromedio;
		$promedio = number_format($promedio,0);
		echo "<p class='promTotal'><center><b>Promedio:</b> ".$promedio."</center></p>";
		
		?>
		
		<!--	Gráfico	-->
		<div id="container" style="width: 75%;">
			<canvas id="canvas"></canvas>
		</div>
		<script>
			var MONTHS = [<?=$usuarios?>];
			var color = Chart.helpers.color;
			var barChartData = {
				labels: [<?=$usuarios?>],
				datasets: [{
					label: 'Calificación',
					borderWidth: 1,
					backgroundColor: ["#dddddd"],
					data: [
						<?=$calificaciones?>
					]
				}]

			};
			window.onload = function() {
				var ctx = document.getElementById('canvas').getContext('2d');
				window.myBar = new Chart(ctx, {
					type: 'bar',
					data: barChartData,
					options: {
						responsive: true,
						legend: {
							position: 'top',
						},
						title: {
							display: true,
							text: 'Chart.js Bar Chart'
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								}
							}]
						}
					}
				});

			};
		</script>

		
		<?php
		
	}

?>

<script>
	jQuery(document).ready(function(){
		jQuery('#encuesta').select2({
			placeholder: 'Seleccione una opción'
		});
		jQuery('#escuela').select2({
			placeholder: 'Seleccione una opción'
		});
	});
</script>