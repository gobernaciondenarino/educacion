<?php

/**
 * @file
 * A module to provide product comparisons.
 */

/**
 * Implements hook_init().
 */
function comparison_init() {
  drupal_add_js(drupal_get_path('module', 'comparison') . '/comparison.js');
  drupal_add_css(drupal_get_path('module', 'comparison') . '/comparison.css');
}

/**
 * Implements hook_permission().
 */
function comparison_permission() {
  return array(
    'administer comparison' => array(
      'title' => t('Administer comparison'),
      'description' => t('Administer comparison settings.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function comparison_menu() {
  $items = array();

  $items['comparison/%node/flag'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'comparison_flag',
    'page arguments' => array(1, 2),
    'access callback' => TRUE,
  );

  $items['comparison/%node/unflag'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'comparison_flag',
    'page arguments' => array(1, 2),
    'access callback' => TRUE,
  );

  $items['comparison/message'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'comparison_message',
    'page arguments' => array(),
    'access callback' => TRUE,
  );

  $items['product-comparison'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'comparison_table',
    'access callback' => TRUE,
  );

  $items['admin/config/comparison'] = array(
    'title' => 'Comparison',
    'description' => 'Comparison modules',
    'access callback' => 'user_access',
    'access arguments' => array('administer comparison'),
  );

  $items['admin/config/comparison/comparison'] = array(
    'title' => 'Comparison settings',
    'description' => 'Configure comparison settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('comparison_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer comparison'),
    'file' => 'comparison.admin.inc',
  );

  return $items;
}

function comparison_table() {
  global $user;
  if ($user->uid == 0) {
    $sid = session_api_get_sid(FALSE);
    $nodes = $_SESSION['comparison']['sid'][$sid];
  }
  else {
    $nodes = $_SESSION['comparison']['uid'][$user->uid];
  }
  $output = '';
  $headers = array();
  $thumbnails = array();
  $titles = array();
  $thumbnails[] = array('data' => variable_get('corner_html', ''));
  $titles[] = array('data' => '', 'header' => TRUE);
  if (count($nodes) > 0) {
    foreach ($nodes as $nid => $value) {
      $field = variable_get('comparison_fields', '');
      $node = node_load($nid);
      $file = NULL;
      if (property_exists($node, $field)) {
        $f = $node->$field;
        if (isset($f['und'][0]['fid'])) {
          $file = file_load($f['und'][0]['fid']);
        }
      }
      if (is_null($file)) {
        $thumbnails[] = array('data' => '');
      }
      else {
        $image_style = variable_get('image_style', '');
        if ($image_style=='') {
          $variables = array(
            'path' => file_create_url($file->uri),
            'attributes' => array('class' => 'comparison_image'),
          );
        }
        else {
          $variables = array(
            'path' => image_style_url($image_style, $file->uri),
            'attributes' => array('class' => $image_style),
          );
        }
        $image = theme('image', $variables);
        $options = array('html' => TRUE);
        $thumbnails[] = array('data' => l($image, 'node/' . $nid, $options));
      }
      $options
        = array('attributes' => array('class' => 'f_prod_comp_prod_title'));
      $titles[] = array('data' => l($node->title, 'node/' . $nid, $options));
    }
  }
  $headers[] = $thumbnails;
  $headers[] = $titles;
  $rows = array();
  $vocabulary = taxonomy_vocabulary_machine_name_load('f_prod_2_feat');
  $tree1 = taxonomy_get_tree($vocabulary->vid, 0, 1);
  foreach ($tree1 as $term1) {
    $row = array();
    $row[] = array('data' => $term1->name, 'class' => array('parent'));
    if (count($nodes) > 0) {
      foreach ($nodes as $nid => $value) {
        $row[] = array('data' => '', 'class' => array('blank'));
      }
    }
    $rows[] = $row;
    $tree2 = taxonomy_get_tree($vocabulary->vid, $term1->tid, 1);
    foreach ($tree2 as $term2) {
      $hide = variable_get('comparison_' . $term2->tid, 0);
      if ($hide) {
        continue;
      }
      $row = array();
      $row[] = array('data' => $term2->name, 'class' => array('child'));
      if (count($nodes) > 0) {
        foreach ($nodes as $nid => $value) {
          $query = db_select('taxonomy_index', 'ti');
          $count = $query
            ->condition('ti.nid', $nid)
            ->condition('ti.tid', $term2->tid)
            ->countQuery()
            ->execute()
            ->fetchField();
          if ($count) {
            $row[] = array('data' => '');
          }
          else {
            $row[] = array('data' => '');
          }
        }
      }
      $row[count($row)-1]['class'][] = 'last';
      $rows[] = $row;
    }
  }
  return theme('header_table', array(
    'headers' => $headers,
    'rows' => $rows,
    'sticky' => TRUE,
  ));
}

/**
 * Implements hook_theme().
 */
function comparison_theme() {
  return array(
    'header_table' => array(
      'variables' => array(
        'headers' => array(),
        'rows' => array(),
      )
    ),
  );
}

function theme_header_table($variables) {
  $headers = $variables['headers'];
  $rows = $variables['rows'];
  $attributes = (isset($variables['attributes'])) ?
    $variables['attributes'] : array();
  $caption = (isset($variables['caption'])) ?
    $variables['caption'] : '' ;
  $colgroups = (isset($variables['colgroups'])) ?
    $variables['colgroups'] : array();
  $sticky = (isset($variables['sticky'])) ?
    $variables['sticky'] : 0;
  $empty = (isset($variables['empty'])) ? $variables['empty'] : FALSE;

  // Add sticky headers, if applicable.
  if (count($headers) && $sticky) {
    drupal_add_js('misc/tableheader.js');
    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $attributes['class'][] = 'sticky-enabled';
  }
  $output = '<table' . drupal_attributes($attributes) . ">\n";

  if (isset($caption)) {
    $output .= '<caption>' . $caption . "</caption>\n";
  }

  // Format the table columns:
  if (count($colgroups)) {
    foreach ($colgroups as $number => $colgroup) {
      $attributes = array();

      // Check if we're dealing with a simple or complex column
      if (isset($colgroup['data'])) {
        foreach ($colgroup as $key => $value) {
          if ($key == 'data') {
            $cols = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cols = $colgroup;
      }

      // Build colgroup
      if (is_array($cols) && count($cols)) {
        $output .= ' <colgroup' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cols as $col) {
          $output .= ' <col' . drupal_attributes($col) . ' />';
        }
        $output .= " </colgroup>\n";
      }
      else {
        $output .= ' <colgroup' . drupal_attributes($attributes) . " />\n";
      }
    }
  }

  // Add the 'empty' row message if available.
  if (!count($rows) && $empty) {
    $header_count = 0;
    foreach ($header as $header_cell) {
      if (is_array($header_cell)) {
        $header_count += isset($header_cell['colspan']) ? $header_cell['colspan'] : 1;
      }
      else {
        $header_count++;
      }
    }
    $rows[] = array(array('data' => $empty, 'colspan' => $header_count, 'class' => array('empty', 'message')));
  }

  // Format the table header:
  $output .= ' <thead>';
  foreach ($headers as $header) {
    if (count($header)) {
      $ts = tablesort_init($header);
      // HTML requires that the thead tag has tr tags in it followed by tbody
      // tags. Using ternary operator to check and see if we have any rows.
      $output .= (count($rows) ? '<tr>' : ' <tr>');
      foreach ($header as $cell) {
        $cell = tablesort_header($cell, $header, $ts);
        $output .= _theme_table_cell($cell, TRUE);
      }
      // Using ternary operator to close the tags based on whether or not there are rows
      $output .= (count($rows) ? " </tr>\n" : "</tr>\n");
    }
    else {
      $ts = array();
    }
  }
  $output .= '</thead>';
  // Format the table rows:
  if (count($rows)) {
    $output .= "<tbody>\n";
    $flip = array('even' => 'odd', 'odd' => 'even');
    $class = 'even';
    foreach ($rows as $number => $row) {
      $attributes = array();

      // Check if we're dealing with a simple or complex row
      if (isset($row['data'])) {
        foreach ($row as $key => $value) {
          if ($key == 'data') {
            $cells = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cells = $row;
      }
      if (count($cells)) {
        // Add odd/even class
        if (empty($row['no_striping'])) {
          $class = $flip[$class];
          $attributes['class'][] = $class;
        }

        // Build row
        $output .= ' <tr' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cells as $cell) {
          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= _theme_table_cell($cell);
        }
        $output .= " </tr>\n";
      }
    }
    $output .= "</tbody>\n";
  }

  $output .= "</table>\n";
  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter() for taxonomy_form_term().
 */
function comparison_form_taxonomy_form_term_alter(&$form, $form_state) {
  $tid = $form['#term']['tid'];
  $form['filter_visibility'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('comparison_' . $tid, 0),
    '#title' => t('Hide this term from all product filters'),
    '#weight' => -1,
  );
  $form['#submit'][] = 'comparison_taxonomy_form_submit';
}

function comparison_taxonomy_form_submit($form, &$form_state) {
  $tid = $form['#term']['tid'];
  variable_set('comparison_' . $tid,
    $form_state['values']['filter_visibility']);
}

/**
 * Implements hook_form_FORM_ID_alter() for views_exposed_form().
 */
function comparison_form_views_exposed_form_alter(&$form, $form_state) {
  $css = '';
  foreach ($form as $key => $value) {
    if (drupal_substr($key, 0, 3) == 'tid') {
      foreach ($value['#options'] as $tid => $term_name) {
        if (variable_get('comparison_' . $tid, 0)) {
          $delta = drupal_substr($key, 4);
          if (drupal_strlen($delta) == 0) {
            $css .= '.views-exposed-form .form-item-edit-tid-' .
              $tid . ' {display: none;}';
          }
          else {
            $css .= '.views-exposed-form .form-item-edit-tid-' .
              drupal_substr($key, 4) . '-' . $tid . ' {display: none;}';
          }
        }
      }
    }
  }
  drupal_add_css($css, 'inline');
}

function comparison_flag($node, $action) {
  global $user;
  if ($user->uid == 0) {
    $sid = session_api_get_sid(TRUE);
    if ($action == 'flag') {
      $_SESSION['comparison']['sid'][$sid][$node->nid] = TRUE;
    }
    else {
      unset($_SESSION['comparison']['sid'][$sid][$node->nid]);
    }
  }
  else {
    $sid = 0;
    if ($action == 'flag') {
      $_SESSION['comparison']['uid'][$user->uid][$node->nid] = TRUE;
    }
    else {
      unset($_SESSION['comparison']['uid'][$user->uid][$node->nid]);
    }
  }
  $result = new stdClass();
  $result->action = $flag;
  $result->status = TRUE;
  $result->sid = $sid;
  $result->uid = $user->uid;
  drupal_json_output($result);
}

function comparison_message() {
  global $user;
  $result = new stdClass();
  if ($user->uid == 0) {
    $sid = session_api_get_sid(FALSE);
    if ($sid == 0) {
      $result->status = FALSE;
      drupal_json_output($result);
      return;
    }
    if (isset($_SESSION['comparison']['sid'][$sid])) {
      $result->nodes = $_SESSION['comparison']['sid'][$sid];
      $result->count = count($_SESSION['comparison']['sid'][$sid]);
      $result->status = TRUE;
    }
    else {
      $result->status = FALSE;
    }
  }
  else {
    if (isset($_SESSION['comparison']['uid'][$user->uid])) {
      $result->nodes = $_SESSION['comparison']['uid'][$user->uid];
      $result->count = count($_SESSION['comparison']['uid'][$user->uid]);
      $result->status = TRUE;
    }
    else {
      $result->status = FALSE;
    }
  }
  drupal_json_output($result);
}

function comparison_node_view($node, $view_mode, $langcode) {
  $view_modes = variable_get('comparison_view_modes', array('teaser'));
  if (in_array($view_mode, $view_modes)) {
    $node_types = variable_get('comparison_node_types', array());
    if (in_array($node->type, $node_types)) {
      $path = base_path() . drupal_get_path('module', 'comparison');
      $node->content['links']['comparison']['#theme'] = 'comparison_link';
      $node->content['links']['comparison']['#links']['node-comparison']
        = array(
          'title' => theme('image', array('path' => $path . '/compare.gif')),
          'href' => 'product-comparison',
          'html' => TRUE,
          'attributes' => array(
           'class' => 'comparison-node comparison-node-' . $node->nid
          ),
        );
      $node->content['links']['comparison']['#attributes']['class']
        = array('links', 'inline');
    }
  }
}
