<?php
	
	//	Exportar encuestas
	if($exportar == 1){
		
		?>
		<style>
			.page-header{
				display:none;
			}
		</style>
		
		<?php
		
		if(!$encuExportSele){
			
			echo "
				<script>
					jQuery('.c_encuesta').removeClass('active');
					jQuery('.a_encuesta').removeClass('active');
				</script>
			";
			
			echo "
				<h2 class='page-header' style='display:block;'>Exportar encuesta</h2>
				<form	action='/encuestas/admin?exportar=1' target='_blank' method='post'>
					<label>Seleccionar encuesta</label>
					<p>
			";
			echo "<select id='encuExport' name='encuExport'>";
			$encuestas = db_query("select * from encuestas where status in (1,3)");
			foreach($encuestas as $encuestasArray){
				if($encuExportSele == $encuestasArray->id){
					echo "<option selected value='".$encuestasArray->id."'>".$encuestasArray->name."</option>";
				}else{
					echo "<option value='".$encuestasArray->id."'>".$encuestasArray->name."</option>";
				}
			}
			echo "</select>";
			echo "<br/><br/><button type='submit'>Exportar</button>";
			echo "
					</p>
				</form>
			";
			
		}else{
			
			header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
			header("Content-type:   application/x-msexcel; charset=utf-8");
			header("Content-Disposition: attachment; filename=Encuestas.xls"); 
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo "<table border='1'>";
			echo "<tr>";
			echo "<td><b>Escuela</b></td>";
			echo "<td><b>Encuesta</b></td>";
			echo "<td><b>Usuario</b></td>";
			echo "<td><b>Pregunta</b></td>";
			echo "<td><b>Respuesta</b></td>";
			echo "</tr>";
			$respuestas = db_query("select * from encuestasRespuestas where encuesta = '".$encuExportSele."' ");
			foreach($respuestas as $respuesta){
				$campo1 = $respuesta->school;
				$campo2 = $respuesta->encuesta;
				$campo3 = $respuesta->user;
				$campo4 = $respuesta->pregunta;
				$campo5 = $respuesta->respuesta;
				
				//	Obtener nombre de la escuela
				$schoolCons = db_query("SELECT title_field_value FROM field_data_title_field where entity_id in (select entity_id from field_revision_field_codigo_dane where field_codigo_dane_value = '".$campo1."' )");
				foreach($schoolCons as $schoolCon){
					$titleSchool = $schoolCon->title_field_value;
				}
				
				//	Obtener nombre de la encuesta
				$encuCons = db_query("select name from encuestas where id = '$campo2'");
				foreach($encuCons as $encuCon){
					$titleEncu = $encuCon->name;
				}
				
				//	Obtener nombre del usuario
				$usuaCons = db_query("select field_nombres_value from field_data_field_nombres where entity_id = '$campo3' ");
				foreach($usuaCons as $usuaCon){
					$usuario = $usuaCon->field_nombres_value;
				}
				
				//	Obtener descripción de la pregunta
				$pregCons = db_query("select description from encuestasPreguntas where id = '$campo4' ");
				foreach($pregCons as $pregCon){
					$descPreg = $pregCon->description;
				}
				
				echo "
					<tr>
						<td>$titleSchool</td>
						<td>$titleEncu</td>
						<td>$usuario</td>
						<td>$descPreg</td>
						<td>$campo5</td>
					</tr>
				";
			}
			echo "</table>";
			
			die();
			
		}
			
	}else{
	
?>

<style>
	hr{
		height:0px;
		border:1px solid #dddddd;
	}
	input{
		width:100%;
	}
	.encuContent{
		width:100%;
	}
	.encuTable{
		display:table;
		width:100%;
		margin-top:2%;
	}
	.encuRow{
		display:table-row;
	}
	.labelEncu{
		color:#ba0613;
	}
	.encuCell{
		display:table-cell;
		padding-left:2%;
		padding-right:2%;
		padding-top:2%;
		padding-bottom:1%;
		font-size:12px;
		border-bottom: 1px solid #dddddd;
	}
	.encuTableTitle{
		text-align:center;
		font-weight:bold;
	}
	.encuAdd{
		margin-bottom:2%;
		margin-top: 2%;
		border:1px solid #dddddd;
		background:#ffffff;
		color:gray;
		padding-left:2%;
		padding-right:2%;
		padding-top:1%;
		padding-bottom:1%;
	}
	.encuAdd:hover{
		opacity:0.4;
		color:#000000;
	}
	.pregAdd{
		margin-bottom:2%;
		margin-top: 2%;
		border:1px solid #dddddd;
		background:#ffffff;
		color:gray;
		padding-left:2%;
		padding-right:2%;
		padding-top:1%;
		padding-bottom:1%;
	}
	.pregAdd:hover{
		opacity:0.4;
		color:#000000;
	}
	.schoolAdd{
		margin-bottom:2%;
		margin-top: 2%;
		border:1px solid #dddddd;
		background:#ffffff;
		color:gray;
		padding-left:2%;
		padding-right:2%;
		padding-top:1%;
		padding-bottom:1%;
	}
	.schoolAdd:hover{
		opacity:0.4;
		color:#000000;
	}
	.encuConfigEdit{
		margin-bottom:2%;
		margin-top: 2%;
		border:1px solid #dddddd;
		background:#ffffff;
		color:gray;
		padding-left:2%;
		padding-right:2%;
		padding-top:1%;
		padding-bottom:1%;
	}
	.encuConfigEdit:hover{
		opacity:0.4;
		color:#000000;
	}
	.encuClear{
		margin-bottom:2%;
		margin-top: 2%;
		border:1px solid #dddddd;
		background: #ffffff!important;
    color: #648E4B!important;
		padding-left:2%;
		padding-right:2%;
		padding-top:1%;
		padding-bottom:1%;
	}
	.encuClear:hover{
		opacity:0.4;
		color:#000000;
	}
	.encuConfig{
		margin-bottom:2%;
		margin-top: 2%;
		border:1px solid #dddddd;
		background:#ffffff;
		color:gray;
		padding-left:2%;
		padding-right:2%;
		padding-top:1%;
		padding-bottom:1%;
	}
	.encuConfig:hover{
		opacity:0.4;
		color:#000000;
	}
	.encuLabel{
		font-size: 11px;
    color: #8c8c8c;
		margin-top:2%;
	}
	.encuInput{
		height: 10px;
    border-top: 0px;
    border-left: 0px;
    border-right: 0px;
    border-color: #dedede;
	}
	.encuText{
		border-top: 0px;
    border-left: 0px;
    border-right: 0px;
    border-color: #dedede;
	}
	.encuSele{
		border-top: 0px;
    border-left: 0px;
    border-right: 0px;
    border-color: #dedede;
	}
	.encuSepa{
		border: 1px solid #dddddd;
    height: 0px;
	}
	.encuMsg{
		color: #3aa761;
    font-weight: bold;
	}
	.encuCellList1{
		width:5%;
	}
	.encuCellList2{
		width:19%;
	}
	.encuCellList3{
		width:10%;
	}
	.encuCenter{
		text-align:center;
	}
	.encuEdit{
	    border: 0;
    background:none;
    color: #3b3939;
    font-size: 13px;
	}
	.encuEdit:hover{
		opacity:0.4;
		color:#000000;
	}
	.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	.modal-content {
		background-color: #fefefe;
		margin: 15% auto; /* 15% from the top and centered */
		padding: 20px;
		border: 1px solid #888;
		width: 80%; /* Could be more or less, depending on screen size */
	}
	.close {
		color: #aaa;
		width:100%;
		text-align:right;
		font-size: 28px;
		font-weight: bold;
	}
	.close:hover,
	.close:focus {
		color: black;
		text-decoration: none;
		cursor: pointer;
	}
	.encuFormConfig{
		margin-top:4%;
		font-size:12px;
		color:gray;
	}
	.page-header{
		display:none;
	}
	.tableFiltro{
		display:table;
		width:100%;
	}
	.rowFiltro{
		display:table-row;
		padding-bottom:3%;
	}
	.cellFiltro{
		display:table-cell;
		margin-bottom:3%;
		padding-right:2%
	}
	.cellFiltro input{
		width:200px;
	}
	.cellFiltro label{
		margin-top:2%;
	}
</style>

<script>

	//	Editar encuesta
	function encuEdit(id, status, name, description,bajaDesde,bajaHasta,bajaColor,mediaDesde,mediaHasta,mediaColor,altaDesde,altaHasta,altaColor){
		jQuery("#regiEncu").show();
		jQuery("#listEncu").hide();
		jQuery("#idEncu").val(id);
		jQuery("#status").val(status);
		jQuery("#name").val(name);
		jQuery("#description").val(description);
		jQuery("#encuCaliBajaDesde").val(bajaDesde);
		jQuery("#encuCaliBajaHasta").val(bajaHasta);
		jQuery("#encuCaliBajaColor").val(bajaColor);
		jQuery("#encuCaliMediaDesde").val(mediaDesde);
		jQuery("#encuCaliMediaHasta").val(mediaHasta);
		jQuery("#encuCaliMediaColor").val(mediaColor);
		jQuery("#encuCaliAltaDesde").val(altaDesde);
		jQuery("#encuCaliAltaHasta").val(altaHasta);
		jQuery("#encuCaliAltaColor").val(altaColor);
		console.log(bajaDesde + " - " + bajaHasta + " - " + bajaColor);
	}
	
	//	Consultar encuesta
	function consEdit(id, status, name, description,bajaDesde,bajaHasta,bajaColor,mediaDesde,mediaHasta,mediaColor,altaDesde,altaHasta,altaColor){
		jQuery("#regiEncu").show();
		jQuery("#listEncu").hide();
		jQuery("#idEncu").val(id);
		jQuery("#status").val(status);
		jQuery("#name").val(name);
		jQuery("#description").val(description);
		jQuery("#encuCaliBajaDesde").val(bajaDesde);
		jQuery("#encuCaliBajaHasta").val(bajaHasta);
		jQuery("#encuCaliBajaColor").val(bajaColor);
		jQuery("#encuCaliMediaDesde").val(mediaDesde);
		jQuery("#encuCaliMediaHasta").val(mediaHasta);
		jQuery("#encuCaliMediaColor").val(mediaColor);
		jQuery("#encuCaliAltaDesde").val(altaDesde);
		jQuery("#encuCaliAltaHasta").val(altaHasta);
		jQuery("#encuCaliAltaColor").val(altaColor);
		
		//	Desactivar campos
		jQuery(".encuEdit").hide();
		jQuery("#agregaPreg").hide();
		jQuery("#titleEncu").html("Consultar preguntas");
		jQuery("#encuCaliBajaDesde").attr("disabled",true);
		jQuery("#encuCaliBajaHasta").attr("disabled",true);
		jQuery("#encuCaliBajaColor").attr("disabled",true);
		jQuery("#encuCaliMediaDesde").attr("disabled",true);
		jQuery("#encuCaliMediaHasta").attr("disabled",true);
		jQuery("#encuCaliMediaColor").attr("disabled",true);
		jQuery("#encuCaliAltaDesde").attr("disabled",true);
		jQuery("#encuCaliAltaHasta").attr("disabled",true);
		jQuery("#encuCaliAltaColor").attr("disabled",true);
		jQuery(".encuConfigEdit").hide();
		
	}
	
	//	Publicar encuesta
	function publicar(id,name,description){
		var confirmar = confirm("¿Desea continuar con la publicación de la encuesta, recuerde que se desactivara la encuesta publicada actualmente?");
		if(confirmar == true){
			window.location="/encuestas/admin?publicar=" + id + "&name=" + name + "&description=" + description;
		}
	}
	
	//	Establecer valores
	function estaValores(id){
		jQuery("#encuModal4").show();
		jQuery("#idValores").val(id);
		jQuery("#nameEncuConfig").val(jQuery('#name').val());
		jQuery("#descEncuConfig").val(jQuery('#description').val());
	}
	
	//	Desactivar encuesta
	function desactivar(id){
		var confirmar = confirm("¿Desea desactivar la encuesta?");
		if(confirmar == true){
			window.location="/encuestas/admin?desactivar=" + id;
		}
	}
	
	//	Editar pregunta
	function pregEdit(id, status, name, description){
		jQuery("#idPreg").val(id);
		jQuery("#statusPreg").val(status);
		jQuery("#namePreg").val(name);
		jQuery("#descriptionPreg").val(description);
		jQuery("#opcionesPreg").val(jQuery("#opciones"+id).val());
		jQuery("#savePreg").html("Actualizar pregunta");
		jQuery("#saveExitPreg").html("Actualizar pregunta y salir");
	}
	
	//	Editar escuela
	function schoolEdit(id, status, codeSchool){
		jQuery("#idSchool").val(id);
		jQuery("#statusSchool").val(status);
		jQuery("#codeSchool").val(codeSchool);
		jQuery(".schoolAdd").html("Guardar escuela");
	}
	
	//	Limpiar encuesta
	function encuClear(){
		window.location='/encuestas/admin?create=1';
	}
	
	//	Limpiar pregunta
	function pregClear(){
		jQuery("#namePreg").val("");
		jQuery("#descriptionPreg").val("");
		jQuery("#opcionesPreg").val("");
	}
	
	//	Limpiar pregunta
	function schoolClear(){
		jQuery("#codeSchool").val("");
	}
	
	//	Ver historial de la encuesta
	function encuHistory(id){
		jQuery(".encuHistory").hide();
		jQuery(".encuHistory"+id).show();
	}
	
	//	Ver historial de las preguntas
	function pregHistory(id){
		jQuery(".pregHistory").hide();
		jQuery(".pregHistory"+id).show();
	}
	
	//	Ver historial de las escuelas
	function schoolHistory(id){
		jQuery(".schoolHistory").hide();
		jQuery(".schoolHistory"+id).show();
	}
	
	//	Cargar registro de preguntas
	function encuPreg(id){
		jQuery("#idEncuPreg").val(id);
		jQuery("#idPreg").val(0);
		jQuery(".pregResu").hide();
		jQuery(".pregResu"+id).show();
		jQuery("#encuRegiPregName").html(jQuery("#name"+id).val());
		jQuery("#encuRegiPregDesc").html(jQuery("#description"+id).val());
	}
	
	//	Cargar registro de ESCUELAS
	function encuSchool(id){
		jQuery("#idEncuSchool").val(id);
		jQuery("#idSchool").val(0);
		jQuery(".schoolResu").hide();
		jQuery(".schoolResu"+id).show();
		jQuery("#encuRegiSchoolName").html(jQuery("#name"+id).val());
		jQuery("#encuRegiSchoolDesc").html(jQuery("#description"+id).val());
	}
	
	//	Desactivar campos
	function desactivarCampos(){
		jQuery("#name").attr("disabled",true);
		jQuery("#description").attr("disabled",true);
		jQuery("#asignaPregunta").html("Consultar preguntas");
		jQuery("#establecerValores").html("Consultar valores");
		jQuery("#publicarEncuesta").hide();
		jQuery(".encuClearForm").hide();
		jQuery("#encuSave").hide();
		jQuery("#encuClearForm").hide();
		jQuery("#editarEncuestas").hide();
	}
	
</script>

<?php if(@$_REQUEST['create']){echo "<h1 class='page-header' style='display:block;'>Crear encuesta</h1>";}?>
<?php if(@$_REQUEST['exportar']){echo "<h1 class='page-header' style='display:block;'>Exportar</h1>";}?>
<?php if(@!$_REQUEST['exportar'] && @!$_REQUEST['create']){echo "<h1 class='page-header' style='display:block;'>Administrar encuestas</h1>";}?>

<!--	Modal de historico	-->
<div id="encuModal" class="modal">
  <div class="modal-content">
    <div class="close">&times;</div>
    <div id="encuContentModal">
			<h2>Histórico de la encuesta</h2>
			<div class="encuTable">
				<div class="encuRow">
					<div class="encuCell encuTableTitle encuCellList3">Usuario</div>
					<div class="encuCell encuTableTitle encuCellList3">Nombre</div>
					<div class="encuCell encuTableTitle encuCellList3">Descripción</div>
					<div class="encuCell encuTableTitle encuCellList3">Opciones</div>
					<div class="encuCell encuTableTitle encuCellList3">Estado</div>
					<div class="encuCell encuTableTitle encuCellList3">Fecha de registro</div>
				</div>
				<?php	
				
					//	Listar historico de las encuestas

					$contHistory = 0;
					$idTmp = "";
					
					foreach($histories as $history){
						
						//	Imprimir registro de la encuesta
						if($idTmp != $history->id){
							$idTmp = $history->id;
							
							//	Consultar fecha de registro de la encuesta
							$consRegiEncu = db_query("select dateRegister from encuestas where id = '".$history->id."'");
							foreach($consRegiEncu as $resuRegiEncu){
								$dateRegister = $resuRegiEncu->dateRegister;
							}
							
							echo "
								<div class='encuRow encuHistory encuHistory".$history->id."'>
									<div class='encuCell encuCenter'>".$nameUser."</div>
									<div class='encuCell'>".$history->name."</div>
									<div class='encuCell'>".$history->description."</div>
									<div class='encuCell'>Se registró la encuesta</div>
									<div class='encuCell encuCenter'>Diseño</div>
									<div class='encuCell encuCenter'>".$dateRegister."</div>
								</div>
							";
						}
						
						$contHistory = $contHistory + 1;
						
						//	Consultar nombre del usuario
						$nameUser = "";
						$nameUserCons = db_query("select name from users where uid = '".$history->userRegister."' ");
						foreach($nameUserCons as $nameUserResult){
							$nameUser = $nameUserResult->name;
						}
						
						if($history->status == 1)
							$status = "Activo";
						else
							$status = "Inactivo";
						
						if($contHistory == 1)
							$etiqueta = "Se registra encuesta";
						else
							$etiqueta = "Se edita la encuesta" . $contHistory;
						
						echo "
							<div class='encuRow encuHistory encuHistory".$history->id."'>
								<div class='encuCell encuCenter'>".$nameUser."</div>
								<div class='encuCell'>".$history->name."</div>
								<div class='encuCell'>".$history->description."</div>
								<div class='encuCell'>Se actualiza la encuesta</div>
								<div class='encuCell encuCenter'>".$status."</div>
								<div class='encuCell encuCenter'>".$history->dateRegister."</div>
							</div>
						";
						
					}
					
					//	Listar historico de las preguntas
					foreach($historiesPreguntas as $history){
						
						//	Consultar nombre del usuario
						$nameUser = "";
						$nameUserCons = db_query("select name from users where uid = '".$history->userRegister."' ");
						foreach($nameUserCons as $nameUserResult){
							$nameUser = $nameUserResult->name;
						}
						
						if($history->status == 1)
							$status = "Activo";
						else
							$status = "Inactivo";
						echo "
							<div class='encuRow pregHistory pregHistory".$history->idEncu."'>
								<div class='encuCell encuCenter'>Se edita una pregunta</div>
								<div class='encuCell encuCenter'>".$nameUser."</div>
								<div class='encuCell'>".$history->name."</div>
								<div class='encuCell'>".$history->description."</div>
								<div class='encuCell'>".$history->opciones."</div>
								<div class='encuCell encuCenter'>".$status."</div>
								<div class='encuCell encuCenter'>".$history->dateRegister."</div>
							</div>
						";
					}
					
				?>
			</div>
		</div>
  </div>
</div>

<!--	Modal de preguntas	-->
<div id="encuModal2" class="modal">
  <div class="modal-content">
    <div class="close">&times;</div>
		<h2 id="encuRegiPregName"></h2>
		<h4 id="encuRegiPregDesc"></h4><hr/>
		<h3 id="titleEncu">Registro de preguntas</h3><br/>
		
		<!--	Agregar nueva pregunta	-->
		<form id="agregaPreg" action="#" method="post" enctype="multipart/form-data">
			<input type="hidden" id="idEncuPreg" name="idEncuPreg" />
			<input type="hidden" id="idPreg" name="idPreg" />
			<input type="hidden" id="nameEncu" name="nameEncu" value="<?=$tituEncu?>" />
			<input type="hidden" id="descEncu" name="descEncu" value="<?=$descEncu?>" />
			<input type="hidden" id="userRegisterPreg" name="userRegisterPreg" value="<?=$user->uid?>" />
			<input type="hidden" id="salir" name="salir" value="0" />
			<label class="encuLabel">Estado</label><br/>
			<select class="encuSele" id="statusPreg" name="statusPreg">
				<option value="1">Activo</option>
				<option value="0">Inactivo</option>
			</select><br/>
			<label class="encuLabel">Pregunta</label><br/>
			<input class="encuInput" type="text" size="50" id="namePreg" name="namePreg" placeholder="Digitar pregunta" required /><br/>
			<label class="encuLabel">Descripción</label><br/>
			<textarea class="encuText" cols="50" rows="3" placeholder="Digitar la descripción de la pregunta" id="descriptionPreg" name="descriptionPreg" required ></textarea><br/>
			<label class="encuLabel">Opciones</label><br/>
			<textarea class="encuText" cols="50" rows="3" placeholder="Digitar las opciones de respuesta para la pregunta" id="opcionesPreg" name="opcionesPreg" required ></textarea><br/>
			<label class="encuLabel">Cargar imagen (.png)</label><br/>
			<input type="file" id="imagePregunta" name="imagePregunta" /><br/>
			<div>
				<button id="savePreg" class="pregAdd" type="submit" >Guardar y Agregar pregunta</button>
				<button id="saveExitPreg" type="button" class="pregAdd" onclick="jQuery('#salir').val(1);submit();" type="submit" >Guardar y Salir</button>
				<button id="clearPreg" class="encuClear" type="button" onclick="pregClear();">Limpiar</button>
			</div>
		</form>
		
		<!--	Listado de preguntas	-->
		<div class="encuTable">
			<div class="encuRow">
				<div class="encuCell encuTableTitle encuCellList2">PREGUNTA</div>
				<div class="encuCell encuTableTitle encuCellList2">DESCRIPCIÓN</div>
				<div class="encuCell encuTableTitle encuCellList2">ESTADO</div>
				<div class="encuCell encuTableTitle encuCellList2">OPCIONES</div>
				<div class="encuCell encuTableTitle encuCellList2">PROCESO</div>
			</div>	
			<?php	
				$cont = 0;
				foreach($preguntas as $pregunta){
					$cont = $cont + 1;
					if($pregunta->status == 1)
						$status = "Activo";
					else
						$status = "Inactivo";
					echo "
						<div class='encuRow pregResu pregResu".$pregunta->idEncu."'>
							<div class='encuCell encuName'>".$pregunta->name."</div>
							<div class='encuCell encuDescription'>".$pregunta->description."</div>
							<div class='encuCell encuCenter encuStatus'>".$status."</div>
							<div class='encuCell encuCenter encuDate'><input type='hidden' value='".$pregunta->opciones."' id='opciones".$pregunta->id."' />".$pregunta->opciones."</div>
							<div class='encuCell encuCenter'>
								<button alt='Editar' title='Editar' id='editarEncuestas' class='encuEdit iconEdit' onclick=\"pregEdit('".$pregunta->id."','".$pregunta->status."','".$pregunta->name."','".$pregunta->description."');\">Editar</button>
								";
								if(file_exists($_SERVER['DOCUMENT_ROOT']."/sites/default/files/encuestas/".$pregunta->id.".png")){
									echo "<button class='encuEdit' onclick=\"window.open('/sites/default/files/encuestas/".$pregunta->id.".png"."');\">Ver imagen</button>";
								}
					echo "
							</div>
						</div>
					";
				}
			?>
		</div>
	</div>
</div>

<!--	Modal de escuelas	-->
<div id="encuModal3" class="modal">
  <div class="modal-content">
    <div class="close">&times;</div>
		<h2 id="encuRegiSchoolName"></h2>
		<h4 id="encuRegiSchoolDesc"></h4><hr/>
		<h3>Asignar escuelas a la encuesta</h3><br/>
		
		<!--	Agregar nueva escuela	-->
		<form action="#" method="post">
			<input type="hidden" id="idEncuSchool" name="idEncuSchool" />
			<input type="hidden" id="idSchool" name="idSchool" />
			<input type="hidden" id="userRegisterSchool" name="userRegisterSchool" value="<?=$user->uid?>" />
			<label class="encuLabel">Estado</label>
			<select class="encuSele" id="statusSchool" name="statusSchool">
				<option value="1">Activo</option>
				<option value="0">Inactivo</option>
			</select>
			<label class="encuLabel">Código de la escuela</label>
			<select id="codeSchool" name="codeSchool" required>
				<option>- Seleccionar -</option>
				<?php
					$consSchools = db_query("select a.field_codigo_dane_value,b.title_field_value from field_revision_field_codigo_dane a, field_data_title_field b where a.entity_id = b.entity_id");
					foreach($consSchools as $consSchool){
						$code = $consSchool->field_codigo_dane_value; 
						$title = $consSchool->title_field_value;
						echo "<option value='$code'>$title</option>";
					}					
				?>
			</select>
			<div>
				<button class="schoolAdd" type="submit" >Agregar escuela</button>
				<button class="encuClear" type="button" onclick="schoolClear();">Limpiar</button>
			</div>
		</form>
		
		<!--	Listado de escuelas	-->
		<div class="encuTable">
			<div class="encuRow">
				<div class="encuCell encuTableTitle encuCellList2">ESCUELA</div>
				<div class="encuCell encuTableTitle encuCellList2">ESTADO</div>
				<div class="encuCell encuTableTitle encuCellList2">PROCESO</div>
			</div>	
			<?php	
				$cont = 0;
				foreach($schools as $school){
					$cont = $cont + 1;
					if($school->status == 1)
						$status = "Activo";
					else
						$status = "Inactivo";

					//	Consultar nombre de la escuela
					$schoolCons = db_query("SELECT title_field_value FROM field_data_title_field where entity_id in (select entity_id from field_revision_field_codigo_dane where field_codigo_dane_value = '".$school->code."' )");
					foreach($schoolCons as $schoolCon){
						$titleSchool = $schoolCon->title_field_value;
					}

					echo "
						<div class='encuRow schoolResu schoolResu".$school->idEncu."'>
							<div class='encuCell encuName encuCenter'>".$titleSchool."</div>
							<div class='encuCell encuCenter encuStatus'>".$status."</div>
							<div class='encuCell encuCenter'>
								<button alt='Editar' title='Editar' class='encuEdit iconEdit' onclick=\"schoolEdit('".$school->id."','".$school->status."','".$school->code."');\">Editar</button>
							</div>
						</div>
					";
				}
			?>
		</div>
	</div>
</div>

<!--	Modal configuración general	-->
<div id="encuModal4" class="modal">
  <div class="modal-content">
    <div class="close">&times;</div>
		<h2>Establecer valores de calificación</h2>
		<form action="#" class="encuFormConfig" method="post">
		
			<?php
			
				//	Consultar valores de color
				$bajaDesde = 0;
				$bajaHasta = 0;
				$bajaColor = "#ffffff";
				$mediaDesde = 0;
				$mediaHasta = 0;
				$mediaColor = "#ffffff";
				$altaDesde = 0;
				$altaHasta = 0;
				$altaColor = "#ffffff";
				
				if($idProceso){
					$consColor = db_query("select * from encuestasConfig where encuesta = '$idProceso' ");
					foreach($consColor as $colorArray){
						$bajaDesde = $colorArray->bajaDesde;
						$bajaHasta = $colorArray->bajaHasta;
						$bajaColor = $colorArray->bajaColor;
						$mediaDesde = $colorArray->mediaDesde;
						$mediaHasta = $colorArray->mediaHasta;
						$mediaColor = $colorArray->mediaColor;
						$altaDesde = $colorArray->altaDesde;
						$altaHasta = $colorArray->altaHasta;
						$altaColor = $colorArray->altaColor;
					}
				}
			
			?>
		
			<input type="hidden" id="idValores" name="idValores" value="0" />
			<input type="hidden" id="nameEncuConfig" name="nameEncuConfig" />
			<input type="hidden" id="descEncuConfig" name="descEncuConfig" />
			<label class="encuLabel">Calificación Baja</label>
			Desde: <input value="<?=$bajaDesde?>" type="number" step="any" min="0" max="10" id="encuCaliBajaDesde" name="encuCaliBajaDesde" /> 
			Hasta: <input value="<?=$bajaHasta?>" type="number" step="any" min="0" max="10" id="encuCaliBajaHasta" name="encuCaliBajaHasta" /> &nbsp; 
			<input value="<?=$bajaColor?>" type="color" id="encuCaliBajaColor" name="encuCaliBajaColor" /><br/>
			<label class="encuLabel">Calificación Media</label>
			Desde: <input value="<?=$mediaDesde?>" type="number"  step="any" min="0" max="10" id="encuCaliMediaDesde" name="encuCaliMediaDesde" /> 
			Hasta: <input value="<?=$mediaHasta?>" type="number" step="any" min="0" max="10" id="encuCaliMediaHasta" name="encuCaliMediaHasta" /> &nbsp; 
			<input value="<?=$mediaColor?>" type="color" id="encuCaliMediaColor" name="encuCaliMediaColor" /><br/>
			<label class="encuLabel">Calificación Alta</label>
			Desde: <input value="<?=$altaDesde?>" type="number" step="any" min="0" max="10" id="encuCaliAltaDesde" name="encuCaliAltaDesde" /> 
			Hasta: <input value="<?=$altaHasta?>" type="number" step="any" min="0" max="10" id="encuCaliAltaHasta" name="encuCaliAltaHasta" /> &nbsp; 
			<input value="<?=$altaColor?>" type="color" id="encuCaliAltaColor" name="encuCaliAltaColor" /><br/>
			<button type="submit" class="encuConfigEdit" >Guardar</button>
		</form>
	</div>
</div>

<!--	Registro de encuestas-->
<div class="encuContent">

	<!--	Mensaje de respuesta	-->
	<p class="encuMsg"><?=$mensaje?></p>

	<?php
		if($capaView1 == "block"){
			echo "
				<script>
					jQuery('.a_encuesta').removeClass('active');
					jQuery('.e_encuesta').removeClass('active');
				</script>
			";
		}else{
			echo "
				<script>
					jQuery('.c_encuesta').removeClass('active');
					jQuery('.e_encuesta').removeClass('active');
				</script>
			";
		}
	?>
	
	<!--	Agregar nueva encuesta	-->
	<form id="regiEncu" action="#" method="post" style="display:<?=$capaView1?>">
		<input type="hidden" id="idEncu" name="idEncu" value="<?=$idEncu?>" />
		<input type="hidden" id="userRegister" name="userRegister" value="<?=$user->uid?>" />
		<input type="hidden" id="status" name="status" value="2" />
		<label class="encuLabel">Título encuesta</label><br/>
		<input class="encuInput" type="text" size="50" id="name" name="name" placeholder="" required /><br/>
		<label class="encuLabel">Descripción encuesta</label><br/>
		<textarea class="encuText" cols="50" rows="3" id="description" name="description" required ></textarea><br/>
		<input type="hidden" id="encuAll" name="encuAll" value="1"/>
		<!--<input type="checkbox" id="encuAll" name="encuAll" /> <div class="encuAllTexto">Aplica para todas las escuelas</div>-->
		<div>
			<!--<button class="encuConfig" type="button" >Establecer valores de calificación</button>-->
			
			<?php
				
				//	Asignar preguntas solo si la encuesta existe
				if($idProceso){
					echo "<button type='button' class='encuAdd encuOpenModal2' onclick=\"encuPreg('$idProceso');\">Asignar Preguntas</button>";
					echo "<button type='button' class='encuAdd' onclick=\"estaValores('$idProceso');\">Establecer valores</button>";
					
					//	Publicar si ya contiene preguntas
					$consValiPreg = db_query("select * from encuestasPreguntas where idEncu = '".$idProceso."' ");
					$contPreg = 0;
					foreach($consValiPreg as $pregValiArray){
						$contPreg = $contPreg + 1;
					}
					if($contPreg>0){
						
						//	Publicar si ya se asignaron valores
						$consValiPreg = db_query("select * from encuestasConfig where encuesta = '".$idProceso."' ");
						$contPreg = 0;
						foreach($consValiPreg as $pregValiArray){
							$contPreg = $contPreg + 1;
						}
						if($contPreg>0){
							echo "<button type='button' class='encuAdd iconPubl' alt='Publicar' title='Publicar' onclick=\"publicar('$idProceso',jQuery('name').val(),jQuery('description').val());\">Publicar</button>";
						}
						
					}
					
				}
				
				echo "<button id='asignaPregunta' type='button' class='encuAdd encuOpenModal2' style='display:none;' onclick=\"encuPreg(jQuery('#idEncu').val());\">Asignar Preguntas</button>";
				echo "<button id='establecerValores' type='button' class='encuAdd' style='display:none;' onclick=\"estaValores(jQuery('#idEncu').val());\">Establecer valores</button>";
				echo "<button id='publicarEncuesta' type='button' class='encuAdd' style='display:none;' onclick=\"publicar(jQuery('#idEncu').val(),jQuery('name').val(),jQuery('description').val());\">Publicar</button>";
				
			?>
			
			<button id="encuSave" class="encuAdd" type="submit" >Guardar</button>
			<button id="encuClearForm" class="encuClear" type="button" onclick="encuClear();">Limpiar</button>
		</div>
	</form>
	
	<!--	Listar encuestas registradas	-->
	<div id="listEncu" style="display:<?=$capaView2?>!important">
	
		<!--	Filtros	-->
		<div class="tableFiltro">
			<div class="rowFiltro">
				<div class="cellFiltro"><label class="labelEncu"># Encuesta:</label></div>
				<div class="cellFiltro"><label class="labelEncu">Nombre:</label></div>
			</div>
			<div class="rowFiltro">
				<div class="cellFiltro"><input class="text-full" type="text" id="encuFiltId" name="encuFiltId" size="2" /></div>
				<div class="cellFiltro"><input type="text" id="encuFiltName" name="encuFiltName" size="10" /></div>
			</div>
			<div class="rowFiltro">
				<div class="cellFiltro"><label class="labelEncu">Descripción:</label></div>
				<div class="cellFiltro"><label class="labelEncu">Estado:</label></div>
			</div>
			<div class="rowFiltro">
				<div class="cellFiltro"><input type="text" id="encuFiltDescription" name="encuFiltDescription" size="10" /></div>
				<div class="cellFiltro"><input type="text" id="encuFiltStatus" name="encuFiltStatus" size="10" /></div>
			</div>
			<div class="rowFiltro">
				<div class="cellFiltro"><label class="labelEncu">Fecha de registro:</label></div>
			</div>
			<div class="rowFiltro">
				<div class="cellFiltro"><input type="text" id="encuFiltDate" name="encuFiltDate" size="10" /></div>
				<div class="cellFiltro">
					<button type="button" id="aplicarFiltro" name="" value="Aplicar" class="btn btn-info form-submit">Aplicar</button>
				</div>
			</div>
		</div>
	
		<div class="encuTable">
			<div class="encuRow">
				<div class="encuCell encuTableTitle encuCellList1">#</div>
				<div class="encuCell encuTableTitle encuCellList2">Nombre</div>
				<div class="encuCell encuTableTitle encuCellList2">Descripción</div>
				<div class="encuCell encuTableTitle encuCellList2">Estado</div>
				<div class="encuCell encuTableTitle encuCellList2">Fecha de registro</div>
				<div class="encuCell encuTableTitle encuCellList2">Proceso</div>
			</div>
			<?php
				$cont = 0;
				foreach($encuestas as $encuesta){
					$cont = $cont + 1; 
					
					//	Validar estados
					switch($encuesta->status){
						case 1:
							$status = "Publicada";
						break;
						case 2:
							$status = "Diseño";
						break;
						case 3:
							$status = "Desactivada";
						break;
					}
					echo "
						<div class='encuRow encuResu'>
							<div class='encuCell encuCenter encuId'>".$cont."</div>
							<div class='encuCell encuName'><input type='hidden' id='name".$encuesta->id."' value='".$encuesta->name."' />".$encuesta->name."</div>
							<div class='encuCell encuDescription'><input type='hidden' id='description".$encuesta->id."' value='".$encuesta->description."' />".$encuesta->description."</div>
							<div class='encuCell encuCenter encuStatus'>".$status."</div>
							<div class='encuCell encuCenter encuDate'>".$encuesta->dateRegister."</div>
							<div class='encuCell encuCenter'>
					";
					
					//	Unicamente puede editar si la encuesta no esta publicada
					if($encuesta->status == 1 || $encuesta->status == 2)
						$class = "iconActive";
					else
						$class = "iconDisabled";
						
						//	Cambiar valores de color
						$consColor = db_query("select * from encuestasConfig where encuesta = '".$encuesta->id."'");
						foreach($consColor as $colorArray){
							$bajaDesde = $colorArray->bajaDesde;
							$bajaHasta = $colorArray->bajaHasta;
							$bajaColor = $colorArray->bajaColor;
							$mediaDesde = $colorArray->mediaDesde;
							$mediaHasta = $colorArray->mediaHasta;
							$mediaColor = $colorArray->mediaColor;
							$altaDesde = $colorArray->altaDesde;
							$altaHasta = $colorArray->altaHasta;
							$altaColor = $colorArray->altaColor;
						}
						
						echo "<button alt='Editar' title='Editar' class='$class encuEdit iconEdit glyphicon glyphicon-edit' onclick=\"encuEdit('".$encuesta->id."','".$encuesta->status."','".$encuesta->name."','".$encuesta->description."','$bajaDesde','$bajaHasta','$bajaColor','$mediaDesde','$mediaHasta','$mediaColor','$altaDesde','$altaHasta','$altaColor');jQuery('#establecerValores').show();jQuery('#asignaPregunta').show();jQuery('#publicarEncuesta').show();\"></button>";//editar
					
					//	Unicamente se puede des activar si la encuesta se encuentra publicada
					if($encuesta->status == 1){
						$class = "iconActive";
					}else{
						$class = "iconDisabled";
					}
						echo "<button alt='Desactivar' title='Desactivar' class='$class encuEdit iconDesa glyphicon glyphicon-remove' onclick=\"desactivar('".$encuesta->id."');\"></button>";//Desactivar
					
					//	Unicamente se puede consultar si la encuesta se encuesta publicada o desactivada
					if($encuesta->status == 1 || $encuesta->status == 3)
						echo "<button alt='Consultar' title='Consultar' class='encuEdit iconCons glyphicon glyphicon-eye-open' onclick=\"consEdit('".$encuesta->id."','".$encuesta->status."','".$encuesta->name."','".$encuesta->description."','".$bajaDesde."','".$bajaHasta."','".$bajaColor."','".$mediaDesde."','".$mediaHasta."','".$mediaColor."','".$altaDesde."','".$altaHasta."','".$altaColor."');jQuery('#establecerValores').show();jQuery('#asignaPregunta').show();jQuery('#publicarEncuesta').show();desactivarCampos();\"></button>";//Consultar
					
					//	Unicamente se puede publicar si la encuesta se encuentra en diseño
					if($encuesta->status == 2){
						echo "<button alt='Publicar' title='Publicar' class='encuEdit iconPubl glyphicon glyphicon-upload' onclick=\"publicar('".$encuesta->id."','".$encuesta->name."','".$encuesta->description."');\"></button>";//Publicar
					}
					
					//	  <button class='encuEdit encuOpenModal3' onclick=\"encuSchool('".$encuesta->id."');\">Escuelas</button>
					
					echo "
								<button alt='Historico' title='Historico' class='encuEdit encuOpenModal iconHist glyphicon glyphicon-time' onclick=\"encuHistory('".$encuesta->id."');pregHistory('".$encuesta->id."');schoolHistory('".$encuesta->id."');\"></button>
							</div>
						</div>
					";
				}
			?>
		</div>
	</div>
</div>

<?php
	//	Autocompletar
	$autoName = "";
	$encuestas = db_query("select * from encuestas");
	foreach($encuestas as $encuestaArrayAuto){
		if($autoName)
			$autoName = $autoName.",'".$encuestaArrayAuto->name."'";
		else
			$autoName = "['".$encuestaArrayAuto->name."'";
	}
	$autoName = $autoName."]";
?>

<script>

	jQuery(".encuOpenModal").click(function(){
		jQuery("#encuModal").toggle();
	});
	
	jQuery(".encuOpenModal2").click(function(){
		jQuery("#encuModal2").toggle();
	});
	
	jQuery(".encuOpenModal3").click(function(){
		jQuery("#encuModal3").toggle();
	});
	
	jQuery(".encuConfig").click(function(){
		jQuery("#encuModal4").toggle();
	});
	
	jQuery(".close").click(function(){
		jQuery("#encuModal").hide();
		jQuery("#encuModal2").hide();
		jQuery("#encuModal3").hide();
		jQuery("#encuModal4").hide();
	});

	
	//	Aplicar filtros
	jQuery("#aplicarFiltro").click(function(){
		
		var digi, val, cont;
		
		//	Código
		jQuery(".encuResu").show();
		digi = jQuery("#encuFiltId").val();
		jQuery(".encuResu").each(function(){
			val = jQuery(this).find(".encuId").html();
			cont = val.split(jQuery(this).val());
			if(jQuery(this).find(".encuId").html().toLowerCase().indexOf(digi) < 0){
				jQuery(this).hide();
			}
		});
		
		//	Nombre
		digi = jQuery("#encuFiltName").val();
		jQuery(".encuResu").each(function(){
			val = jQuery(this).find(".encuName").html();
			cont = val.split(jQuery(this).val());
			if(jQuery(this).find(".encuName").html().indexOf(digi) < 0){
				jQuery(this).hide();
			}
		});
		
		//	Descripción
		digi = jQuery("#encuFiltDescription").val();
		jQuery(".encuResu").each(function(){
			val = jQuery(this).find(".encuDescription").html();
			cont = val.split(jQuery(this).val());
			if(jQuery(this).find(".encuDescription").html().indexOf(digi) < 0){
				jQuery(this).hide();
			}
		});
		
		//	Estado
		digi = jQuery("#encuFiltStatus").val();
		jQuery(".encuResu").each(function(){
			val = jQuery(this).find(".encuStatus").html();
			cont = val.split(jQuery(this).val());
			if(jQuery(this).find(".encuStatus").html().indexOf(digi) < 0){
				jQuery(this).hide();
			}
		});
		
		//	Fecha
		digi = jQuery("#encuFiltDate").val();
		jQuery(".encuResu").each(function(){
			val = jQuery(this).find(".encuDate").html();
			cont = val.split(jQuery(this).val());
			if(jQuery(this).find(".encuDate").html().indexOf(digi) < 0){
				jQuery(this).hide();
			}
		});
		
	});
	
	jQuery(document).ready(function(){
		jQuery("#encuFiltName").autocomplete({
			source: <?=$autoName?>
		});
		document.title = '<?=$titleWeb?>';
	});
	
	//	Validar proceso de asignar preguntas
	<?php
		if($proceso == 1){
	?>
			
			encuEdit('<?=$idProceso?>','2','<?=$tituEncu?>','<?=$descEncu?>');
	<?php
		}
		if($proceso == 2){
	?>
			
			encuEdit('<?=$idProceso?>','2','<?=$tituEncu?>','<?=$descEncu?>');
			jQuery("#encuModal2").show();
			encuPreg('<?=$idProceso?>');
	<?php
		}
	?>
	
</script>

<?php
	}
?>