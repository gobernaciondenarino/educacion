(function ($) { 
  Drupal.behaviors.HideThisBlock = {
    attach: function() {
      $('a.hide-this-block-ajax').click(function() {
        var a = this;
        href = $(this).attr('href');
        $.ajax({
          type: 'POST',
          data: { js: 1 },
          url: href,
          global: true,
          success: function (data) {
            $(a).parents('.block').slideUp();
          },
          error: function(data) {}
        });
        return false;
      });
    }
  };
})(jQuery);
